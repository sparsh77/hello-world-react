import {useState, useEffect} from "react";


export default function ApiTask() {

    const [data, setData] = useState(null);
    const fetchURL = "https://instapreps.com/api"
    const getData = () => fetch(`${fetchURL}/country_code`)
        .then((response) => response.json())
    useEffect(() => {
        getData().then((response) =>setData(response.data))
    }, []);

    return <div className="container w-75 mx-auto">
        <table className="text-center table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th>Id</th>
                <th>Code</th>
                <th>Country</th>
                <th>Publish</th>
            </tr>
            </thead>
            <tbody>
            {data?.map( (item, index) => {
                return( <tr key={ index }>
                        <td>{item.id}</td>
                        <td>{item.code}</td>
                        <td>{item.country}</td>
                        <td>{item.publish ? 'True' : 'False'}</td>
                    </tr>
            )
            })}
            </tbody>
        </table>
    </div>;




}


//https://instapreps.com/api/country_code
