import 'bootstrap/dist/css/bootstrap.min.css';
import ApiTask from "./ApiTask";


function App() {
  return (
    <div className="App">
        <ApiTask />
    </div>
  );
}

export default App;
